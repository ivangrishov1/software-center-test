document.addEventListener("DOMContentLoaded", function () {
    let linksNav = document.querySelectorAll('.nav__link');

    linksNav.forEach(item => {
        item.addEventListener('click', function() {
            deleteClassActive()

            this.classList.add('nav__link--active')
        })
    })

    function deleteClassActive() {
        linksNav.forEach(item => item.classList.remove('nav__link--active'))
    }
})